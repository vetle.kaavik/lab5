package datastructure;

import labyrinth.LabyrinthTile;

/**
 * ILabyrinthTileGrid is a grid of Labyrinth Tiles
 * 
 * @author Anna Eilertsen - anna.eilertsen@uib.no
 */
public interface ILabyrinthTileGrid extends GridDimension {

	/**
	 * Set the contents of the cell in the given position.
	 * <p>
	 * row must be greater than or equal to 0 and less than rows().
	 * col must be greater than or equal to 0 and less than cols().
	 * 
	 * @param pos     The position of the cell to change the contents of.
	 * @param element The contents the cell is to have.
	 */
	void set(CellPosition pos, LabyrinthTile element);

	/**
	 * Get the contents of the cell in the given position.
	 * <p>
	 * row must be greater than or equal to 0 and less than rows().
	 * col must be greater than or equal to 0 and less than cols().
	 * 
	 * @param pos the position of the cell to get the contents of.
	 */
	LabyrinthTile get(CellPosition pos);

	/**
	 * Make a copy
	 * 
	 * @return A shallow copy of the grid, with the same elements
	 */
	ILabyrinthTileGrid copy();
}
